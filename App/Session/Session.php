<?php

namespace App\Session;
use App\Models\UsersModel;

class Session extends UsersModel
{
    /**
     * créer la session de l'utilisateur
     */
    public function setSession(): void
    {
        $_SESSION["user"] = ["id" => $this->id, "email" => $this->email];
    }
}