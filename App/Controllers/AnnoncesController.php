<?php

namespace App\Controllers;
use App\Models\AnnoncesModel;

class AnnoncesController extends Controller
{
    private AnnoncesModel $annoncesModel;
    public function __construct()
    {
        $this->annoncesModel = new AnnoncesModel();
    }

    /**
     * Affche toutes les annonces actives de la bdd et je génère la vue
     */
    public function index()
    {
        $annonces = $this->annoncesModel->findBy(["actif" => 1]);
        $this->render("annonces/index", compact("annonces"));
    }

    /**
     * Affiche une annonce
     * @param int $id de l'annonce
     */
    public function read(int $id)
    {
        $annonce = $this->annoncesModel->find($id);

        $this->render("annonces/read", compact("annonce"));
    }

}