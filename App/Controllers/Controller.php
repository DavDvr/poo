<?php

namespace App\Controllers;

abstract class Controller
{
    /**
     * J'extrait les données et transmets à ma vue
     * @param string $view
     * @param array $data
     * @param string $template
     */
    public function render(string $view, array $data = [], string $template = 'template')
    {
        extract($data);
        //buffer de sortie
        ob_start();
        require_once (ROOT."/Views/".$view.".php");
        //transfert les informations stockées dans le buffer dans la variable $content pour le fihcier template.php
        $content =ob_get_clean();
        require_once(ROOT . "/Views/template/".$template.".php");
    }
}
