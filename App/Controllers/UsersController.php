<?php

namespace App\Controllers;

use App\CoreApp\Form;
use App\Models\UsersModel;
use App\Session\Session;

class UsersController extends Controller
{
    private Form $form;
    private UsersModel $userModel;
    private Session $session;

    public function __construct()
    {
        $this->form = new Form();
        $this->userModel = new UsersModel();
        $this->session = new Session();
    }

    /**
     * Create form login
     */
    public function login()
    {
       if( Form::validate($_POST, ["email", "password"])){
            $this->userModel->userExist();
        }
      $this->form
        ->startForm()
          ->addLabelForm("email","Email:")
          ->addInput("email", "email", ["class"=> "form-control", "id"=>"email"])
          ->addLabelForm('password', "Password:")
          ->addInput("password", "password", ["id"=>"password", "class"=>"form-control"])
          ->addButton("Login", ["class"=>"btn btn-dark form-control mt-2"])
        ->endForm();

        $this->render("users/login", ["loginForm"=> $this->form->create()]);
    }

    /**
     * Déconnexion de l'utilisateur
     */
    public function logout()
    {
        unset($_SESSION["user"]);
        header("Location: ".$_SERVER['HTTP_REFERER']);
        exit();
    }

    /**
     * Create form login
     */
    public function register()
    {
        //verifie si le form est valide
        if (Form::validate($_POST, ["email", "password"])){
            //nettoie l'adresse mail
            $email = strip_tags($_POST["email"]);
            //chiffre le password
            $pass = password_hash($_POST["password"], PASSWORD_ARGON2I);
            //stock user in bdd
            $this->userModel
                ->setEmail($email)
                ->setPassword($pass);
            $this->userModel->create();
        }
        $this->form
            ->startForm()
            ->addLabelForm("email", "Email:")
            ->addInput("email", "email", ["id" =>"email", "class"=>"form-control"])
            ->addLabelForm("password", "Password:")
            ->addInput("password", "password", ["id" => "password", "class"=>"form-control"])
            ->addButton("Send My Register", ["class"=>"btn btn-dark btn-block form-control mt-2"])
            ->endForm();

        $this->render('users/register', ["loginRegister"=> $this->form->create()]);
    }
}
