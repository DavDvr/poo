<?php

namespace App\CoreApp;
use App\Controllers\MainController;

/**
 * Permet de rediriger vers le bon controller en prenant en compte les différents paramètres de l'url
 * Class Main
 * @package App\CoreApp
 */
class Main
{
    /**
     * redirect trailing slash
     */
    protected function trailingSlashUrl()
    {
        $uri = $_SERVER["REQUEST_URI"];

        if(!empty($uri) && $uri != '/' && $uri[-1] === '/'){
            // Dans ce cas on enlève le /
            $uri = substr($uri, 0, -1);
            // On envoie une redirection permanente
            http_response_code(301);
            // On redirige vers l'URL dans /
            header('Location: '.$uri);
            exit;
        }
    }

    /**
     * je gère les params d'url
     * p=controller/method/params
     * je sépare les params dans un tableau
     * @return false|string[]
     */
    protected function params()
    {
        return $params = explode("/", $_GET['p']);
    }


    /**
     * //namespace / 1ère lettre en maj / et j'ajoute Controller
     * @return string
     */
    protected function recupController(): string
    {
        $params = $this->params();
        return "\\App\\Controllers\\".ucfirst(array_shift($params))."Controller";
    }

    /**
     * Verifie si le controller existe
     */
    protected function classExists()
    {
        if (class_exists($this->recupController())) {
            // je recupère le controller
            $controller = $this->recupController();
            //Je retourne et instancie le controller
            return $controller = new $controller;
        } else {
            echo "Le controller n'existe pas <br>";
        }
    }

    /**
     * Je recupère le controller:  //namespace / 1ère lettre en maj / et j'ajoute Controller.
     * Et l'action : si j'ai pas de deuxième param (method) alors ce sera la methode index.
     * je verifie si la classe du controller existe si oui j'instancie le controller avec son namespace.
     * Je verifie si la methode existe.
     * Si oui, s'il reste des params, j'appelle la methode call_user_func_array en lui passant dans un tableau
     * le controller puis la methode et prendra les params.
     * Sinon j'appelle juste la methode.
     * Si la methode n'existe pas alors erreur 404.
     * @param array $params
     */
    protected function methodControle(array $params)
    {
        $controller = "\\App\\Controllers\\".ucfirst(array_shift($params))."Controller";
        $method = (isset($params[0])) ? array_shift($params) : "index";

        if ($this->classExists()) {
            $controller = new $controller();
        }
        if (method_exists($controller, $method)) {
            (isset($params[0])) ? call_user_func_array([$controller, $method], $params) : $controller->$method();
        } else {
            http_response_code(404);
            echo "La methode n'existe pas";
        }
    }

    /**
     * Permet de démarrer le routeur
     */
    public function start()
    {
        session_start();
        $this->trailingSlashUrl();
        $this->params();
        if (isset($_GET['p'])) {

            if (!empty($this->params()[0])) {
                //au moins 1 paramètre
                $this->methodControle($this->params());
            } else {
                //pas de params j'instancie le controller par defaut
                $mainController = new MainController();
                $mainController->index();
            }
        }
    }
}
