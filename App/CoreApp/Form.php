<?php

namespace App\CoreApp;

class Form
{
    private string $formCode = "";

    /**
     * Return le form html
     * @return string
     */
    public function create()
    {
        return $this->formCode;
    }

    /**
     * Verifie si les champs dont remplis
     * @param array $form du form
     * @param array $fields du form
     * @return bool
     */
    public static function validate(array $form, array $fields): bool
    {
        foreach ($fields as $field){
            if (!isset($form[$field]) || empty($form[$field])){
                return false;
            }
        }
        return true;
    }

    /**
     * Ajoute les attributs envoyés à la balise
     * @param array $attrs
     * @return string
     */
    private function addAttr(array $attrs = []): string
    {
        $str = "";
        //short attr
        $short = ['readonly', 'checked', 'disabled', 'required', 'multiple', 'autofocus', 'novalidate', 'formnovalidate'];
        //je boucle sur les attributs
        foreach ($attrs as $attr => $value){
            // si dans le tableau $short j'ai l'attribut et que ca valeur est true, je le met dans le tableau $str
            if (in_array($attr, $short) && $value == true){
                $str .= " $attr";
            }else{
                $str .= " $attr='$value'";
            }
        }
        return $str;
    }

    /**
     * La balise de départ du formulaire
     * @param string $method du form
     * @param string $action du form
     * @param array $attrs du form
     * @return Form
     */
    public function startForm(string $method = 'post', string $action = "#", array $attrs = []): self
    {
        $this->formCode .= "<form action='$action' method='$method'";
        $this->formCode .= $attrs ? $this->addAttr($attrs) . '>' : '>';
        return $this;
    }

    /**
     * Balise de fin du form
     * @return $this
     */
    public function endForm(): self
    {
        $this->formCode .= '</form>';
        return $this;
    }

    /**
     * Ajoute un label
     * @param string $for
     * @param string $text
     * @param array $attrs
     * @return Form
     */
    public function addLabelForm(string $for, string $text, array $attrs = []):self
    {
        $this->formCode .= "<label for='$for'";
        $this->formCode .= $attrs ? $this->addAttr($attrs) : "";
        $this->formCode .= ">$text</label>";
        return $this;
    }

    /**
     * @param string $type
     * @param string $name
     * @param array $attrs
     * @return $this
     */
    public function addInput(string $type, string $name, array $attrs = []):self
    {
        $this->formCode .= "<input type='$type' name='$name'";
        $this->formCode .= $attrs ? $this->addAttr($attrs).">" : ">";
        return $this;
    }

    /**
     * Ajoute un textarea
     * @param string $name
     * @param string $value
     * @param array $attrs
     * @return $this
     */
    public function addTextArea(string $name, string $value = '', array $attrs = []): self
    {
        $this->formCode .= "<textarea name='$name'";
        $this->formCode .= $attrs ? $this->addAttr($attrs) : "";
        $this->formCode .= ">$value</textarea>";
        return $this;
    }

    /**
     * Ajoute un select
     * @param string $name
     * @param array $options
     * @param array $attrs
     * @return $this
     */
    public function addSelect(string $name, array $options, array $attrs = []):self
    {
        $this->formCode .= "select name='$name'";
        $this->formCode .= $attrs ? $this->addAttr($attrs).">" : ">";
        foreach ($options as $value => $text){
            $this->formCode .= "<option value='$value'>$text</option>";
        }
        $this->formCode .= "</select>";
        return $this;
    }

    public function addButton(string $text, array $attrs = []): self
    {
        $this->formCode .= "<button ";
        $this->formCode .= $attrs ? $this->addAttr($attrs) : "";
        $this->formCode .= ">$text</button>";
        return $this;
    }
}









