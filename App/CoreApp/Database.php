<?php

namespace App\CoreApp;

use PDO;
use PDOException;

class Database extends PDO
{

    private static $instanceDb;
    private const DBHOST = 'localhost:8889';
    private const DBUSER = 'root';
    private const DBPASS= 'Cd6vdph';
    private const DBNAME = 'poo';

    /**
     * Database constructor.
     */
    private function __construct()
    {
        $dsn = 'mysql:dbname='. self::DBNAME .'; host='.self::DBHOST;
        try {
            parent::__construct($dsn,self::DBUSER,self::DBPASS);
            $this->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8' );
            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            die($e->getMessage());
        }

    }

    /**
     * return Instance Database Singleton
     * @return static
     */
    public static function getInstanceDB(): self
    {
        if (is_null(self::$instanceDb)){

            self::$instanceDb = new self();
        }
        return self::$instanceDb;
    }
}
