<?php if (!empty($_SESSION["error"])):?>
    <div class="alert alert-danger mt-5" role="alert">
      <?php echo $_SESSION["error"]; unset($_SESSION["error"]); ?>
    </div>
<?php endif;?>
<h1 class="text-center text-dark mt-3">Login</h1>
<div class="container mx-auto">
<?= $loginForm ?>
<a class="text-muted text-decoration-none" aria-current="page" href="http://localhost:8888/MVCAnnonces/App/public/users/register">No Register?</a>

</div>
