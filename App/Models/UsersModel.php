<?php

namespace App\Models;

use App\Session\Session;

class UsersModel extends Model
{
    protected int $id;
    protected string $email;
    protected string $password;

    /**
     * UsersModel constructor.
     * @param int $id
     * @param string $email
     * @param string $password
     */
    public function __construct(int $id= 1, string $email= "", string $password= "")
    {
        $this->nameTable(__CLASS__);
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UsersModel
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UsersModel
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

}