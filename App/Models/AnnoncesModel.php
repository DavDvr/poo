<?php

namespace App\Models;
use DateTime;

class AnnoncesModel extends Model
{

    protected int $id;
    protected string $titre;
    protected string $description;
    protected DateTime $created_at;
    protected bool $actif;

    /**
     * AnnoncesModel constructor.
     */
    public function __construct()
    {
        $this->nameTable(__CLASS__);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     * @return AnnoncesModel
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return AnnoncesModel
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime $created_at
     * @return AnnoncesModel
     */
    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActif(): bool
    {
        return $this->actif;
    }

    /**
     * @param bool $actif
     * @return AnnoncesModel
     */
    public function setActif(bool $actif): self
    {
        $this->actif = $actif;
        return $this;
    }


}