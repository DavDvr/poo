<?php

namespace App\Models;

use App\CoreApp\Database;
use App\Session\Session;
use PDOStatement;

class Model extends Database
{

    /**
     * @var string name of the table
     */
    protected string $table;

    /**
     * Return the name of the table
     * @param $class
     * @return string
     */
    public function nameTable($class): string
    {
        $className = str_replace(__NAMESPACE__.'\\', "", $class);
        return $this->table = strtolower(str_replace("Model", "", $className));
    }

    /**
     * @var Database $db
     */
    private Database $db;

    /**
     * Give me all the data to the table
     * @return array
     */
    public function findAll(): array
    {
        $query = $this->getQuery("select * from ". $this->table);
        return $query->fetchAll();
    }

    /**
     * Give me the data with this criteria
     * @param array $criteria
     * @return array
     */
    public function findBy(array $criteria): array
    {
        $fields = [];
        $values = [];
        // array associative
        foreach ($criteria as $field => $value){
            //select * from annonces where actif = ? and id = 1;
            //bindValues(1, value)
            $fields[] = "$field = ?";
            $values[] = $value;
        }
        // i modify the array "flieds" by a string
        $list_fields = implode(" AND ", $fields);

        // i execute the request
        return $this->getQuery('SELECT * FROM '. $this->table. ' WHERE '.$list_fields, $values)->fetchAll();

    }

    /**
     * Give me the data with id = $data or email
     * @param int|string $data
     * @return object
     */
    public function find(int|string $data): object
    {
        if (is_int($data)){

            $user= (object)$this->getQuery("SELECT * FROM  {$this->table}  WHERE id= {$data}")->fetch();
            return $user;

        }elseif(is_string($data)){

               $user = (object) $this->getQuery("SELECT * FROM  {$this->table} WHERE email= '{$data}'")->fetch();
               return $user;
        }
    }

    /**
     * @return PDOStatement
     */
    public function create(): PDOStatement
    {
        $fields = [];
        $inter = [];
        $values = [];
        // array associative
        foreach ($this as $field => $value){
            //insert into annonces (titre, description, actif) values(?, etc..);
            if ($value !== null && $field != "db" && $field != "table") {
                $fields[] = $field;
                $inter[]= "?";
                $values[] = $value;
            }
        }
        // i implode the list_fields by "," and i implode list_inter by ","
        $list_fields = implode(", ", $fields);
        $list_inter = implode(", ", $inter);
        // i execute the request
        return $this->getQuery('INSERT INTO '. $this->table. ' ('. $list_fields .') VALUES('. $list_inter. ')', $values);

    }

    /**
     * @return PDOStatement
     */
    public function update(): PDOStatement
    {
        $fields = [];
        $inter = [];
        $values = [];
        // array associative
        foreach ($this as $field => $value){
            //UPDATE annonces SET titre = ?, description = ?, actif = ? WHERE id= ?
            if ($value !== null && $field != "database" && $field != "table") {
                $fields[] = "$field = ?";
                $values[] = $value;
            }
        }
        $values[] = $this->id;
        // i implode the list_fields by ","
        $list_fields = implode(", ", $fields);
        // i execute the request
        return $this->getQuery('UPDATE '. $this->table . ' SET '. $list_fields. ' WHERE id = ? ', $values);

    }

    public function delete(int $id)
    {
        $this->getQuery('DELETE FROM '. $this->table. ' WHERE id = ? ', [$id] );
    }

    /**
     * Allow to know if the request is prepare() or simple query()
     * @param string $sql
     * @param array|null $attributs
     * @return PDOStatement
     */
    protected function getQuery(string $sql, ?array $attributs=null): PDOStatement
    {
        //je recupère l'instance de Database
        $this->db = Database::getInstanceDB();

        if ($attributs !== null){
            $query = $this->db->prepare($sql);
            $query->execute($attributs);
            return $query;
        }else{
            return $this->db->query($sql);
        }
    }

    /**
     * @param object|array $datas
     * @return Model
     */
    public function hydrate (object|array $datas): Model
    {
        foreach ($datas as $key => $value){
            //i keep the setter corresponding at the $key
            // titre -> setTitre()
            $setter = 'set'. ucfirst($key);
            //if the setter exist
            if (method_exists($this, $setter)){
                // i call the setter
                $this->$setter($value);
            }
        }
        return $this;
    }

    /**
     * Verifie si l'utilisateur existe en BDD avec son email et password
     */
    public function userExist(){
        $session = new Session();
        $user = $this->find(htmlentities($_POST["email"]));
        //si pas d'utilisateur
        if (!$user){
            //envoie message de session
            $_SESSION["error"] = "Credentials isn't correct";
            header("Location: http://localhost:8888/MVCAnnonces/App/public/users/login");
            exit();
        }
        //sinon
        $user = $this->hydrate($user);
        //vefif password
        if (password_verify($_POST["password"], $user->getPassword())){
            $session->setSession();
            header("Location: http://localhost:8888/MVCAnnonces/App/Public/main");
        }else{
            $_SESSION["error"] = "Credentials isn't correct";
            header("Location: http://localhost:8888/MVCAnnonces/App/Public/users/login");
            exit();
        }
    }

}