<?php
define('ROOT', dirname(__DIR__));
require_once ROOT ."/vendor/autoload.php";
//je definie une constante contenant le dossier racine du projet
use App\CoreApp\Main;

$app = new Main();
$app->start();
